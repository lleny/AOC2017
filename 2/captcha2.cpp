#include <iostream>
#include <string.h>

using namespace std;

int main( int argc, char *argv[] ){
    //solution int
    int solution = 0;
    //data length
    int slen = strlen(argv[1]);
    //half length
    int hlen = slen/2;

    // for each char in input string
    for( int i = 0; i < slen; i++ ) {
            // check the current char against the one halfway around
            if (argv[1][i] == argv[1][(i + hlen) % slen ]){
                //cout << "comparing " << i << " with " << (i + hlen) % slen << endl;
                // add to solution
                solution += static_cast<int>(argv[1][i] - '0');
            }
    }
    // check ends
    //if ( argv[1][strlen(argv[1])-1] == argv[1][0])
    //   solution += static_cast<int>(argv[1][0] - '0');

    cout << solution << endl;
}
