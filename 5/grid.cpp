#include <iostream>
#include <cmath>
using namespace std;

// Function to find The Bounding Odd Root, is the first odd number >= the root of input
int boundingOddRoot( int input ) {
    
    // Get the root of input
    double decimalRoot = sqrt(input);
    // Get the ceiling of that
    int ceilRoot = ceil(decimalRoot);
    // If even, add one to make odd, else return 
    if( ceilRoot % 2 == 0 )
        return ceilRoot+1;
    return ceilRoot;
}

// Function to Go around the circle  until we find the closest midpoint
int distanceFromMidpoint( int input, int bottomRightDiag, int circleNumber) {
    int currentMidpoint = bottomRightDiag - circleNumber;
    for( int i = 0 ; i < 4; i++ ) {
        // We know that if distance from a midpoint to the input is less than circleNumber(half a side width)
        // Then we know its the closest midpoint
        if( abs(input - currentMidpoint) < circleNumber ) 
            return abs(input - currentMidpoint); 
        // or else do the next midpoint
        else 
            currentMidpoint -= (2*circleNumber);
    }
    // This is a bad function that doesn't have another return, but that shouldn't ever happen probably
} 


int main( int argc, char* argv[]) {

int input = 368078;


// Calculate the bounding odd root for the next three numbers
int BOD = boundingOddRoot(input);
// The bottom right Diagonal location is the BOD squared 
int bottomRightDiag = BOD*BOD;
// The circle number of the input is the floor of BOD/2 (this is distance from center to solution's perimeter
int circleNumber = floor((double)BOD/(double)2);


// Find the distance from input to the closest midpoint using above numbers

int distance = distanceFromMidpoint( input, bottomRightDiag, circleNumber); 

// The solution is the distance from a midpoint on the side of a perimeter, 
// plus the distance from center (circleNumber
cout << distance + circleNumber << endl;

}
