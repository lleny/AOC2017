#include <fstream>
#include <iostream>
#include <sstream>
#include <vector>
#include <string>

using namespace std;

vector< vector<int> >openDataFile( char *fileName) {

    ifstream dataFile(fileName);
    vector< vector<int> > returnData;
    string str; 

    // For each line in data file
    while(getline(dataFile, str)){
        // For each 
        //for( int i = 0; i < str.length(); i++ ) {
        istringstream iss(str);
        string element;
        vector<int> dataLine;
        while( iss >> element) {
            if( element != " " )
                dataLine.push_back(stoi(element));
        }
        returnData.push_back(dataLine);
    }
     

    return returnData;
}



int main( int argc, char *argv[]) {
    vector< vector<int> > data = openDataFile(argv[1]);
    int solution = 0;
    int div = 0;

    // For each row of data
    for( int i = 0; i < data.size(); i++ ) {
        // For each element
        for( int j = 0; j < data[i].size(); j++ ) {
            // For each element after 
            for( int k = j+1; k < data[i].size(); k++ ) {
                //Mod compare the bigger element
                if( data[i][j] > data[i][k]){
                    if( data[i][j] % data[i][k] == 0 )
                        div = data[i][j] / data[i][k];
                }
                else
                    if( data[i][k] % data[i][j] == 0 )
                        div = data[i][k] / data[i][j];

            }
        }
        // Add to solution
        solution += div; 
        
    }

    cout << solution << endl;
}


