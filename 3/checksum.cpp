#include <fstream>
#include <iostream>
#include <sstream>
#include <vector>
#include <string>

using namespace std;

vector< vector<int> >openDataFile( char *fileName) {

    ifstream dataFile(fileName);
    vector< vector<int> > returnData;
    string str; 

    // For each line in data file
    while(getline(dataFile, str)){
        // For each 
        //for( int i = 0; i < str.length(); i++ ) {
        istringstream iss(str);
        string element;
        vector<int> dataLine;
        while( iss >> element) {
            if( element != " " )
                dataLine.push_back(stoi(element));
        }
        returnData.push_back(dataLine);
    }
     

    return returnData;
}



int main( int argc, char *argv[]) {
    vector< vector<int> > data = openDataFile(argv[1]);
    int solution = 0;

    // For each row of data
    for( int i = 0; i < data.size(); i++ ) {
        int high = 0;
        int low = 99999;
        for( int j = 0; j < data[i].size(); j++ ) {
            // Find high
            if( data[i][j] > high )
                high = data[i][j];
            // Find low
            if( data[i][j] <  low )
                low = data[i][j];

        }
        // Find difference
        int difference = high - low;
        solution += difference; 
        
    }

    cout << solution << endl;
}


