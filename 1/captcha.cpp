#include <iostream>
#include <string.h>

using namespace std;

int main( int argc, char *argv[] ){
   //cout << argv[1][0] << " " << argv[1][1] << endl; 

   //cout << strlen(argv[1]) << endl;
    //solution int
    int solution = 0;

    // for each char in input string
    for( int i = 0; i < strlen(argv[1]); i++ ) {
            // check the current char against the next one
            if (argv[1][i] == argv[1][i+1])
                // add to solution
                solution += static_cast<int>(argv[1][i] - '0');
    }
    // check ends
    if ( argv[1][strlen(argv[1])-1] == argv[1][0])
       solution += static_cast<int>(argv[1][0] - '0');

    cout << solution << endl;
}
